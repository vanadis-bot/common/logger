package com.vanadis.logger.logback

//import com.vanadis.logger.logback.extension.Logging
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.full.companionObject

inline fun <reified R : Any> R.logger(): Logger = LoggerFactory.getLogger(
        getClassForLogging(this::class.java)
)

//inline fun <reified T : Logging> T.logger(): Logger
//        = LoggerFactory.getLogger(getClassForLogging(T::class.java))

inline fun <T : Any> getClassForLogging(javaClass: Class<T>): Class<*> {
    return javaClass.enclosingClass?.takeIf {
        it.kotlin.companionObject?.java == javaClass
    } ?: javaClass
}