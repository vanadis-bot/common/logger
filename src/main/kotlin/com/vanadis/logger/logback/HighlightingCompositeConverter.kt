package com.vanadis.logger.logback

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.pattern.color.ANSIConstants
import ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase

class HighlightingCompositeConverter : ForegroundCompositeConverterBase<ILoggingEvent>() {

    override fun getForegroundColorCode(event: ILoggingEvent): String {
        val level = event.level

        when (level.toInt()) {
            Level.ERROR_INT -> {
                // same as default color scheme
                return ANSIConstants.BOLD + ANSIConstants.RED_FG
            }
            Level.WARN_INT -> {
                // same as default color scheme
                return ANSIConstants.YELLOW_FG
            }
            Level.INFO_INT, Level.DEBUG_INT -> {
                // same as default color scheme
                return ANSIConstants.BLUE_FG
            }
            else -> {
                return ANSIConstants.DEFAULT_FG
            }
        }
    }
}
