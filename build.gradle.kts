plugins {
    kotlin("jvm")
}

group = "vanadis.common.logger"
version = "1.0.0"

dependencies {

    implementation(kotlin("stdlib"))

    implementation(kotlin("reflect"))

    // Use the Kotlin test library.
//    testImplementation(kotlin("kotlin-test"))

    // Use the Kotlin JUnit integration.
//    testImplementation(kotlin("kotlin-test-junit"))

    /**
     * Logging
     */

    // https://mvnrepository.com/artifact/org.slf4j/slf4j-api
    api("org.slf4j", "slf4j-api", "1.7.25")

    // https://mvnrepository.com/artifact/ch.qos.logback/logback-core
    api("ch.qos.logback", "logback-core", "1.2.3")

    // https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
    api("ch.qos.logback", "logback-classic", "1.2.3")
}